// @include "../../jquery.js"

var favStreamers = [
  "Lekkerspelen",
  "xqcow",
  "ethotv",
  "ukf",
  "ml7support",
  "esportsprimelol",
  "stormen",
]

var lessFav = [
  "hachubby",
  "GetQuakedOn",
  "dannedd",
  "emongg",
  "brandito",
  "redshell",
  "eskay",
  "vastility",
  "faide",
  "karq",
  "philza",
  "sweetdreams",
  "m0xyy",
  "dizzy",
  "reckful",
  "summit1g",
  "forsen",
  "codcode904",
  "NymN",
  "shroud",
  "arrge",
  "michaelreeves",
  "greekgodx",
  "yeatle",
  "sodapoppin",
  "mizkif",
  "aceu",
  "boomboomflocke"
]


function setStreamerPrio(arr, cssClass) {
  console.log("loaded fav streamers");
  $(".live-channel-card")
    .parent()
    .each(function (i) {
      var steamers = arr.map((v) => v.toLowerCase());
      var stream = $(this);
      var url = $(this).find($('a[data-a-target="preview-card-title-link"]')).attr("href");
      var name = url.substr(1);

      for (let i = 0; i < steamers.length; i++) {
        var streamer = steamers[i];

        if (streamer == name) {
          stream.addClass(cssClass);
        }
      }
    });
}

$(document).on("click", function () {
  setStreamerPrio(favStreamers, 'fav-streamer');
  setStreamerPrio(lessFav, 'less-fav-streamer');
});

if (window.location.href == "https://www.twitch.tv/directory/following/live") {
  var checkExist = setInterval(function () {
    if ($(".live-channel-card").length) {
      setStreamerPrio(favStreamers, 'fav-streamer');
      setStreamerPrio(lessFav, 'less-fav-streamer');

      clearInterval(checkExist);
    }
  }, 100); // check every 100ms
}